import 'package:cash_book/widgets/app_color.dart';
import 'package:cash_book/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyDialog {
  AppColor appColor = AppColor();

  showWarning({
    required BuildContext context,
    required String message,
    IconData? icon,
    Color? colorsIcon,
  }) async {
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  icon ?? Icons.warning,
                  color: colorsIcon ?? AppColor().mainColor,
                  size: fixSize * 0.045,
                ),
                SizedBox(
                  height: fixSize * 0.01,
                ),
                CustomText(
                  text: message,
                  color: Colors.black,
                  fontSize: fixSize * 0.0185,
                )
              ],
            ),
            actions: [
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => AppColor().mainColor)),
                        onPressed: () {
                          Get.back();
                        },
                        child: CustomText(
                          text: 'ປິດ',
                          fontSize: fixSize * 0.0185,
                          color: Colors.white,
                        )),
                  ),
                ],
              )
            ],
          );
        });
  }

  showWarningDialog({
    required String message,
    IconData? icon,
    Color? colorsIcon,
  }) async {
    await Get.dialog(Builder(builder: (context) {
      Size size = MediaQuery.of(context).size;
      double fixSize = size.width + size.height;
      return AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              icon ?? Icons.warning,
              color: colorsIcon ?? AppColor().mainColor,
              size: fixSize * 0.045,
            ),
            SizedBox(
              height: fixSize * 0.01,
            ),
            CustomText(
              text: message,
              color: Colors.black,
              fontSize: fixSize * 0.0185,
            )
          ],
        ),
        actions: [
          Row(
            children: [
              Expanded(
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.resolveWith(
                            (states) => AppColor().mainColor)),
                    onPressed: () {
                      Get.back();
                    },
                    child: CustomText(
                      text: 'ປິດ',
                      fontSize: fixSize * 0.0185,
                      color: Colors.white,
                    )),
              ),
            ],
          )
        ],
      );
    }));
  }

  showSuccess({
    required BuildContext context,
    required String message,
    IconData? icon,
    Color? colorsIcon,
  }) async {
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  icon ?? Icons.check_box,
                  color: colorsIcon ?? AppColor().green,
                  size: fixSize * 0.045,
                ),
                SizedBox(
                  height: fixSize * 0.01,
                ),
                CustomText(
                  text: message,
                  color: Colors.black,
                  fontSize: fixSize * 0.0185,
                )
              ],
            ),
            actions: [
              SizedBox(
                width: size.width,
                child: Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith(
                            (states) => AppColor().green,
                          )),
                          onPressed: () {
                            Get.back();
                          },
                          child: CustomText(
                            text: 'ປິດ',
                            fontSize: fixSize * 0.0185,
                            color: Colors.white,
                          )),
                    ),
                  ],
                ),
              )
            ],
          );
        });
  }

  showSignUpDialogSuccess({
    required String message,
    IconData? icon,
    Color? colorsIcon,
  }) async {
    await Get.dialog(Builder(builder: (context) {
      Size size = MediaQuery.of(context).size;
      double fixSize = size.width + size.height;
      return AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              icon ?? Icons.check_box,
              color: colorsIcon ?? AppColor().green,
              size: fixSize * 0.045,
            ),
            SizedBox(
              height: fixSize * 0.01,
            ),
            CustomText(
              text: message,
              color: Colors.black,
              fontSize: fixSize * 0.0185,
            )
          ],
        ),
        actions: [
          SizedBox(
            width: size.width,
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.resolveWith(
                        (states) => AppColor().green,
                      )),
                      onPressed: () {
                        Get.back();
                      },
                      child: CustomText(
                        text: 'ປິດ',
                        fontSize: fixSize * 0.0185,
                        color: Colors.white,
                      )),
                ),
              ],
            ),
          )
        ],
      );
    }));
  }

  dialogLoad({
    required BuildContext context,
  }) async {
    await showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return Material(
            color: Colors.transparent,
            child: Center(
              child: Container(
                width: fixSize * 0.125,
                height: fixSize * 0.125,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(fixSize * 0.0075)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(
                      color: appColor.mainColor,
                    ),
                    SizedBox(
                      height: fixSize * 0.015,
                    ),
                    CustomText(
                      text: 'ກຳລັງໂຫລດ..',
                      color: appColor.mainColor,
                      fontSize: fixSize * 0.015,
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  showLogoutDialog({required BuildContext context}) async {
    var value = await showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            title: Icon(
              Icons.exit_to_app,
              color: appColor.mainColor,
              size: fixSize * 0.05,
            ),
            content: CustomText(
                text: 'do_you_want_to_logout?', fontSize: fixSize * 0.0185),
            actionsPadding: EdgeInsets.symmetric(horizontal: fixSize * 0.015),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => Colors.grey)),
                        onPressed: () {
                          Get.back();
                        },
                        child: CustomText(
                          text: 'cancel',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  ),
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => appColor.mainColor)),
                        onPressed: () {
                          Get.back(result: 'ok');
                        },
                        child: CustomText(
                          text: 'ok',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  )
                ],
              )
            ],
          );
        });
    return value;
  }

  showConfirmOrderFromCustomer({required BuildContext context}) async {
    var value = await showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            title: Icon(
              Icons.move_to_inbox,
              color: appColor.mainColor,
              size: fixSize * 0.05,
            ),
            content: CustomText(
                text: 'ທ່ານຕ້ອງການຮັບອໍເດີນິ້ແທ້ບໍ່?', fontSize: fixSize * 0.0185),
            actionsPadding: EdgeInsets.symmetric(horizontal: fixSize * 0.015),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => Colors.grey)),
                        onPressed: () {
                          Get.back();
                        },
                        child: CustomText(
                          text: 'ຍົກເລີກ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  ),
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => appColor.mainColor)),
                        onPressed: () {
                          Get.back(result: 'ok');
                        },
                        child: CustomText(
                          text: 'ຕົກລົງ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  )
                ],
              )
            ],
          );
        });
    return value;
  }

  showPaymentDialog({required BuildContext context}) async {
    var value = await showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            title: Icon(
              Icons.payment,
              color: appColor.mainColor,
              size: fixSize * 0.05,
            ),
            content: CustomText(
                text: 'ກວດສອບ ແລະ ຢືນຢັນການຊຳລະ', fontSize: fixSize * 0.0185),
            actionsPadding: EdgeInsets.symmetric(horizontal: fixSize * 0.015),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => Colors.grey)),
                        onPressed: () {
                          Get.back();
                        },
                        child: CustomText(
                          text: 'ຍົກເລີກ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  ),
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => appColor.mainColor)),
                        onPressed: () {
                          Get.back(result: 'ok');
                        },
                        child: CustomText(
                          text: 'ຊຳລະ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  )
                ],
              )
            ],
          );
        });
    return value;
  }

  showUpdateStockProduct({required BuildContext context}) async {
    var value = await showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            title: Icon(
              Icons.payment,
              color: appColor.mainColor,
              size: fixSize * 0.05,
            ),
            content:
                CustomText(text: 'ໃສ້ຈຳນວນສິນຄ້າ!', fontSize: fixSize * 0.0185),
            actionsPadding: EdgeInsets.symmetric(horizontal: fixSize * 0.015),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => Colors.grey)),
                        onPressed: () {
                          Get.back();
                        },
                        child: CustomText(
                          text: 'ຍົກເລີກ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  ),
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => appColor.mainColor)),
                        onPressed: () {
                          Get.back(result: 'ok');
                        },
                        child: CustomText(
                          text: 'ຊຳລະ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  )
                ],
              )
            ],
          );
        });
    return value;
  }

  showConfrimOrderToTableSame({required BuildContext context}) async {
    var value = await showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          double fixSize = size.width + size.height;
          return AlertDialog(
            title: Icon(
              Icons.table_bar,
              color: appColor.mainColor,
              size: fixSize * 0.05,
            ),
            content: CustomText(
                text: 'ໂຕະນີ້ໃຊ້ງານຢູ່ ທ່ານຕ້ອງການເພີ່ມສິນຄ້າໃນບິນນີ້ແທ້ບໍ່!',
                fontSize: fixSize * 0.0185),
            actionsPadding: EdgeInsets.symmetric(horizontal: fixSize * 0.015),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => Colors.grey)),
                        onPressed: () {
                          Get.back();
                        },
                        child: CustomText(
                          text: 'ຍົກເລີກ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  ),
                  SizedBox(
                    width: fixSize * 0.095,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => appColor.mainColor)),
                        onPressed: () {
                          Get.back(result: 'ok');
                        },
                        child: CustomText(
                          text: 'ຕົກລົງ',
                          color: appColor.white,
                          fontSize: fixSize * 0.0185,
                        )),
                  )
                ],
              )
            ],
          );
        });
    return value;
  }
}
