import 'package:flutter/material.dart';

class AppColor {
  Color mainColor = Colors.blue;
  Color secondColor = Colors.orange.shade100;
  Color thirdColor = Colors.orange.shade200;
  Color white = Colors.white;
  Color black = Colors.black;
  Color grey = Colors.grey;
  Color red = Colors.red;
  Color green = Colors.green;
  Color blue = Colors.blue;
}
