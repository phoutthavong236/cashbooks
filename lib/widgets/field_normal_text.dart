import 'package:flutter/material.dart';

class FieldNormalTextWidget extends StatelessWidget {
  const FieldNormalTextWidget(
      {Key? key,
      required this.hintText,
      required this.textController,
      this.obscure = false,
      this.keyBoardType = TextInputType.emailAddress,
      this.useEye})
      : super(key: key);

  final TextEditingController textController;
  final String hintText;

  final bool obscure;
  final TextInputType keyBoardType;
  final bool? useEye;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double fSize = size.width + size.height;

    return TextFormField(
      controller: textController,
      obscureText: obscure,
      textAlignVertical: TextAlignVertical.center,
      cursorColor: const Color.fromRGBO(145, 200, 65, 1),
      keyboardType: keyBoardType,
      decoration: InputDecoration(
        suffixIcon: useEye == null
            ? null
            : useEye!
                ? IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.remove_red_eye,
                      color: Colors.grey,
                      size: fSize * 0.0185,
                    ))
                : null,
        enabledBorder: OutlineInputBorder(
            gapPadding: 0,
            borderRadius: BorderRadius.circular(fSize * 0.005),
            borderSide: const BorderSide(width: 1, color: Colors.blue)),
        fillColor: Colors.white,
        filled: true,
        hintText: hintText,
        hintStyle: TextStyle(fontSize: fSize * 0.0135),
        focusedBorder: OutlineInputBorder(
            gapPadding: 0,
            borderRadius: BorderRadius.circular(fSize * 0.005),
            borderSide: const BorderSide(width: 1, color: Colors.blue)),
        focusColor: Colors.grey,
      ),
    );
  }
}
