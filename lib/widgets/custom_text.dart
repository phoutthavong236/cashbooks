
import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  const CustomText(
      {Key? key,
      required this.text,
      this.fontSize,
      this.color,
      this.textAlign,
      this.fontWeight,
      this.textDecoration,
      this.shadows})
      : super(key: key);
  final String text;
  final double? fontSize;
  final Color? color;
  final TextDecoration? textDecoration;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final List<Shadow>? shadows;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        decoration: textDecoration ?? TextDecoration.none,
        fontSize: fontSize,
        color: color,
        shadows: shadows,
        fontWeight: fontWeight,
        fontFamily: 'saysettha'
      ),
    );
  }
}
