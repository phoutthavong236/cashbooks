// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyAYvsnJDZA3lRyuPdsAAdcRsZl7QiqrtMo',
    appId: '1:797479574445:web:9cdca0445ce6ab81819f24',
    messagingSenderId: '797479574445',
    projectId: 'cash-book-fb2cf',
    authDomain: 'cash-book-fb2cf.firebaseapp.com',
    storageBucket: 'cash-book-fb2cf.appspot.com',
    measurementId: 'G-CZ2B2JGFF5',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyCCHAuV5q2baY-Ip4LGZa6olNfYtExohqw',
    appId: '1:797479574445:android:896c365f30f77870819f24',
    messagingSenderId: '797479574445',
    projectId: 'cash-book-fb2cf',
    storageBucket: 'cash-book-fb2cf.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyDBVxzTXLmgL1OwYkppPz60hhQx6VevXrQ',
    appId: '1:797479574445:ios:803101c923711dfd819f24',
    messagingSenderId: '797479574445',
    projectId: 'cash-book-fb2cf',
    storageBucket: 'cash-book-fb2cf.appspot.com',
    iosClientId: '797479574445-gn1hcil82hjc2mqju14e8fqmrjho5ngr.apps.googleusercontent.com',
    iosBundleId: 'com.example.cashBook',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyDBVxzTXLmgL1OwYkppPz60hhQx6VevXrQ',
    appId: '1:797479574445:ios:803101c923711dfd819f24',
    messagingSenderId: '797479574445',
    projectId: 'cash-book-fb2cf',
    storageBucket: 'cash-book-fb2cf.appspot.com',
    iosClientId: '797479574445-gn1hcil82hjc2mqju14e8fqmrjho5ngr.apps.googleusercontent.com',
    iosBundleId: 'com.example.cashBook',
  );
}
