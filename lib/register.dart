import 'package:cash_book/cash_book.dart';
import 'package:cash_book/states/profile_state.dart';
import 'package:cash_book/widgets/custom_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widgets/field_text_widget.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController nameT = TextEditingController();
  TextEditingController emailT = TextEditingController();
  TextEditingController passT = TextEditingController();
  ProfileState profileState = Get.put(ProfileState());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double fixSize = size.width + size.height;
    return Scaffold(
      body: SizedBox(
        width: size.width,
        height: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: size.width * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FieldTextWidget(
                    hintText: 'name',
                    icon: Icons.person,
                    textController: nameT,
                    obscure: false,
                  ),
                  SizedBox(
                    height: fixSize * 0.01,
                  ),
                  FieldTextWidget(
                    hintText: 'Email',
                    icon: Icons.email,
                    textController: emailT,
                    obscure: false,
                  ),
                  SizedBox(
                    height: fixSize * 0.01,
                  ),
                  FieldTextWidget(
                    hintText: 'Password',
                    icon: Icons.lock,
                    textController: passT,
                    obscure: true,
                  ),
                  SizedBox(
                    height: fixSize * 0.01,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: fixSize * 0.035,
                          child: ElevatedButton(
                              onPressed: () {
                                logincheck();
                              },
                              child: const CustomText(
                                text: 'Register',
                              )),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> logincheck() async {
    if (nameT.text.trim().isNotEmpty &&
        emailT.text.isNotEmpty &&
        passT.text.isNotEmpty) {
      try {
        await FirebaseFirestore.instance
            .collection('users')
            .where('email', isEqualTo: emailT.text)
            .get()
            .then((value) {
          if (value.docChanges.isEmpty) {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: CustomText(
              text: "Email ນີ້ ມີຢູ່ໃນລະບົບແລ້ວ",
              color: Colors.white,
            )));
            return;
          }
        });

        await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
                email: emailT.text, password: passT.text)
            .then((value) async {
          Map<String, dynamic> map = {
            'name': nameT.text.trim(),
            'email': emailT.text.trim(),
            'password': passT.text.trim(),
            'time': Timestamp.now(),
          };
          await FirebaseFirestore.instance
              .collection('users')
              .doc(FirebaseAuth.instance.currentUser!.uid)
              .set(map)
              .then((value) {
            Get.offAll(const CashBookPage());
          });
        });
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: CustomText(
          text: "ບາງຢ່າງຜິດພາດ ກະລຸນາລອງໃຫມ່!",
          color: Colors.white,
        )));
        await FirebaseAuth.instance.signOut();
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: CustomText(
        text: "ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ",
        color: Colors.white,
      )));
    }
  }
}
