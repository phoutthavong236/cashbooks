import 'package:cash_book/cash_book_detail.dart';
import 'package:cash_book/login_page.dart';
import 'package:cash_book/models/cash_book_model.dart';
import 'package:cash_book/states/profile_state.dart';
import 'package:cash_book/widgets/app_color.dart';
import 'package:cash_book/widgets/custom_text.dart';
import 'package:cash_book/widgets/field_normal_text.dart';
import 'package:cash_book/widgets/format_price.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CashBookPage extends StatefulWidget {
  const CashBookPage({Key? key}) : super(key: key);

  @override
  State<CashBookPage> createState() => _CashBookPageState();
}

class _CashBookPageState extends State<CashBookPage> {
  ProfileState profileState = Get.put(ProfileState());
  TextEditingController titleT = TextEditingController();
  Map<String, dynamic> totalAll = {};
  List<CashBookModel> cashBooksList = [];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double fixSize = size.width + size.height;
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: 'Cash Book',
          fontSize: fixSize * 0.015,
        ),
      ),
      body: SizedBox(
        width: size.width,
        height: size.height,
        child: Column(
          children: [
            SizedBox(
              height: fixSize * 0.01,
            ),
            StreamBuilder<List<CashBookModel>>(
                stream: getDataCashBook(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  return Container(
                      padding: EdgeInsets.all(fixSize * 0.015),
                      margin: EdgeInsets.symmetric(horizontal: fixSize * 0.01),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: fixSize * 0.0025)
                          ],
                          borderRadius: BorderRadius.circular(fixSize * 0.0025),
                          color: Colors.white),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomText(
                                  text: 'ເງີນລວມທັງໝົດ',
                                  fontSize: fixSize * 0.0165,
                                  fontWeight: FontWeight.w600,
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomText(
                                  text: 'ເງີນລວມ',
                                  fontSize: fixSize * 0.015,
                                ),
                                CustomText(
                                  text: FormatPrice(
                                          price: totalAll['totalAll'] ?? 0)
                                      .toString(),
                                  fontSize: fixSize * 0.015,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: fixSize * 0.0025,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomText(
                                  text: 'ເງີນໃຊ້ຈ່າຍ',
                                  fontSize: fixSize * 0.015,
                                ),
                                CustomText(
                                  text:
                                      '-${FormatPrice(price: totalAll['spendAll'] ?? 0)}',
                                  fontSize: fixSize * 0.015,
                                  color: Colors.red,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: fixSize * 0.0025,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomText(
                                  text: 'ເງີນຍັງເຫຼືອ',
                                  fontSize: fixSize * 0.015,
                                ),
                                CustomText(
                                  text: FormatPrice(
                                          price: totalAll['balanceAll'] ?? 0)
                                      .toString(),
                                  fontSize: fixSize * 0.015,
                                  color: Colors.green,
                                ),
                              ],
                            )
                          ]));
                }),
            Expanded(
              child: StreamBuilder<List<CashBookModel>>(
                  stream: getDataCashBook(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    return GridView.builder(
                        padding:
                            EdgeInsets.symmetric(horizontal: fixSize * 0.01),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: size.width > 800 ? 4 : 2,
                          crossAxisSpacing: fixSize * 0.005,
                          mainAxisSpacing: fixSize * 0.005,
                          mainAxisExtent: fixSize * 0.155,
                        ),
                        itemCount: snapshot.data!.length + 1,
                        itemBuilder: (context, index) {
                          if (index == snapshot.data!.length) {
                            return InkWell(
                              onTap: () {
                                titleT.clear();
                                Get.dialog(AlertDialog(
                                  title: CustomText(
                                    text: 'ໃສ່ຫົວຂໍ້: ',
                                    fontSize: fixSize * 0.0165,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  content: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        FieldNormalTextWidget(
                                            hintText: 'ໃສ່ຫົວຂໍ້',
                                            textController: titleT)
                                      ]),
                                  actions: [
                                    ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.resolveWith(
                                                  (states) => Colors.grey)),
                                      onPressed: () {
                                        Get.back();
                                      },
                                      child: CustomText(
                                        text: 'ຍົກເລິກ',
                                        fontSize: fixSize * 0.0165,
                                      ),
                                    ),
                                    ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.resolveWith(
                                                  (states) => Colors.blue)),
                                      onPressed: () {
                                        if (titleT.text.trim().isNotEmpty) {
                                          createdNewCashBook();
                                        }
                                      },
                                      child: CustomText(
                                        text: 'ຕົກລົງ',
                                        fontSize: fixSize * 0.0165,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ));
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: fixSize * 0.01),
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: fixSize * 0.0025)
                                    ],
                                    borderRadius:
                                        BorderRadius.circular(fixSize * 0.01),
                                    color: Colors.white),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.blue,
                                  size: fixSize * 0.05,
                                ),
                              ),
                            );
                          }

                          return InkWell(
                            onLongPress: () {
                              showDialogDelete(snapshot.data![index].id!);
                            },
                            onTap: () {
                              Get.to(CashBookDetail(
                                  id: snapshot.data![index].id!));
                            },
                            child: Container(
                              padding: EdgeInsets.all(fixSize * 0.005),
                              margin: EdgeInsets.only(top: fixSize * 0.01),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: fixSize * 0.0025)
                                  ],
                                  borderRadius:
                                      BorderRadius.circular(fixSize * 0.01),
                                  color: Colors.white),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Wrap(
                                      children: [
                                        CustomText(
                                          text:
                                              'ຫົວຂໍ້: ${snapshot.data![index].title}',
                                          fontSize: fixSize * 0.015,
                                          fontWeight: FontWeight.w600,
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: fixSize * 0.0125,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        CustomText(
                                          text: 'ເງີນລວມ',
                                          fontSize: fixSize * 0.0125,
                                        ),
                                        CustomText(
                                          text: FormatPrice(
                                                  price: snapshot
                                                          .data![index].total ??
                                                      0)
                                              .toString(),
                                          fontSize: fixSize * 0.0135,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: fixSize * 0.005,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        CustomText(
                                          text: 'ເງີນໃຊ້ຈ່າຍ',
                                          fontSize: fixSize * 0.0125,
                                        ),
                                        CustomText(
                                          text:
                                              '-${FormatPrice(price: snapshot.data![index].spend ?? 0)}',
                                          fontSize: fixSize * 0.0135,
                                          color: Colors.red,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: fixSize * 0.005,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        CustomText(
                                          text: 'ເງີນຍັງເຫຼືອ',
                                          fontSize: fixSize * 0.0125,
                                        ),
                                        CustomText(
                                          text: FormatPrice(
                                                  price: snapshot.data![index]
                                                          .balance ??
                                                      0)
                                              .toString(),
                                          fontSize: fixSize * 0.0135,
                                          color: Colors.green,
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          );
                        });
                  }),
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            Container(
              color: Colors.blue,
              width: size.width,
              height: fixSize * 0.135,
              child: GetBuilder<ProfileState>(builder: (get) {
                if (get.userModel == null) {
                  return const Center(child: CircularProgressIndicator());
                }
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                      text: get.userModel!.name ?? '',
                      fontSize: fixSize * 0.0165,
                      color: Colors.white,
                    ),
                    CustomText(
                      text: get.userModel!.email ?? '',
                      fontSize: fixSize * 0.0165,
                      color: Colors.white,
                    ),
                  ],
                );
              }),
            ),
            Expanded(
              child: SizedBox(
                width: size.width,
                child: Stack(
                  children: [
                    Positioned(
                      right: 0,
                      left: 0,
                      bottom: fixSize * 0.015,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (states) => Colors.blue)),
                            onPressed: () async {
                              await FirebaseAuth.instance.signOut();
                              Get.off(const LoginPage());
                            },
                            child: CustomText(
                              text: 'ອອກຈາກລະບົບ',
                              fontSize: fixSize * 0.015,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Stream<List<CashBookModel>> getDataCashBook() async* {
    cashBooksList = [];
    yield* FirebaseFirestore.instance
        .collection('cashBooks')
        .where('user_id', isEqualTo: FirebaseAuth.instance.currentUser?.uid)
        .snapshots()
        .map((snapShot) {
      cashBooksList = snapShot.docs
          .map((document) => CashBookModel.fromJson(document.data()))
          .toList();
      getDataAllTotal();
      return cashBooksList;
    });
  }

  getDataAllTotal() async {
    num total = 0;
    num spend = 0;
    num balance = 0;
    for (var element in cashBooksList) {
      total += element.total ?? 0;
      spend += element.spend ?? 0;
    }
    balance = total - spend;
    Map<String, dynamic> map = {
      'totalAll': total,
      'spendAll': spend,
      'balanceAll': balance
    };

    totalAll = map;
  }

  showDialogDelete(String id) async {
    var res = await Get.dialog(Builder(builder: (context) {
      Size size = MediaQuery.of(context).size;
      double fixSize = size.width + size.height;
      return AlertDialog(
        title: Icon(
          Icons.question_mark_outlined,
          color: AppColor().mainColor,
          size: fixSize * 0.025,
        ),
        content: CustomText(
          text: 'ທ່ານຕ້ອງການລົບຫົວຂໍ້ນີ້ຫຼືບໍ່ ',
          fontSize: fixSize * 0.0165,
          fontWeight: FontWeight.w600,
          textAlign: TextAlign.center,
        ),
        actions: [
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.grey)),
            onPressed: () {
              Get.back();
            },
            child: CustomText(
              text: 'ຍົກເລິກ',
              fontSize: fixSize * 0.0165,
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.blue)),
            onPressed: () {
              Get.back(result: true);
            },
            child: CustomText(
              text: 'ຕົກລົງ',
              fontSize: fixSize * 0.0165,
              color: Colors.white,
            ),
          ),
        ],
      );
    }));
    if (res == true) {
      await FirebaseFirestore.instance.collection('cashBooks').doc(id).delete();
    }
  }

  createdNewCashBook() async {
    String id = FirebaseFirestore.instance.collection('cashBooks').doc().id;
    Map<String, dynamic> map = {
      'id': id,
      "title": titleT.text.trim(),
      "total": 0,
      "spend": 0,
      "balance": 0,
      "user_id": FirebaseAuth.instance.currentUser?.uid,
      "listdetail": []
    };
    Get.back();
    await FirebaseFirestore.instance
        .collection('cashBooks')
        .doc(id)
        .set(map)
        .then((value) async {
      print('success');
    }).catchError((e) {
      print(e.toString());
    });
  }
}
