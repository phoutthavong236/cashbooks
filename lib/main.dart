import 'package:cash_book/cash_book.dart';
import 'package:cash_book/firebase_options.dart';
import 'package:cash_book/login_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cash Book',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const CheckAuthPage(),
    );
  }
}

class CheckAuthPage extends StatefulWidget {
  const CheckAuthPage({Key? key}) : super(key: key);

  @override
  State<CheckAuthPage> createState() => _CheckAuthPageState();
}

class _CheckAuthPageState extends State<CheckAuthPage> {
  var user;

  @override
  void initState() {
    checkLoginAuth();
    super.initState();
  }

  checkLoginAuth() async {
    FirebaseAuth.instance.authStateChanges().listen((user) async {
      this.user = user;
      if (user == null) {
        print('User is currently signed out!');
         Get.off(const LoginPage());
      } else {
        print('User is signed in!');
        var data = await FirebaseFirestore.instance
            .collection('users')
            .doc(user.uid)
            .get();
        Get.off(const CashBookPage());
      }

      if (mounted) setState(() {});
    });

    //  checkStatus = saveAuthLogin.getStorage.read('Login') ?? 'null';
  }

  @override
  Widget build(BuildContext context) {
    return const SizedBox();
  }
}
