import 'package:cash_book/widgets/app_color.dart';
import 'package:cash_book/widgets/custom_text.dart';
import 'package:cash_book/widgets/field_normal_text.dart';
import 'package:cash_book/widgets/format_price.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'models/cash_book_model.dart';

class CashBookDetail extends StatefulWidget {
  const CashBookDetail({Key? key, required this.id}) : super(key: key);
  final String id;

  @override
  State<CashBookDetail> createState() => _CashBookDetailState();
}

class _CashBookDetailState extends State<CashBookDetail> {
  TextEditingController amountT = TextEditingController();
  TextEditingController descript = TextEditingController();
  late CashBookModel cashBookModel;
  DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm');
  List<dynamic> listdetail = [];
  AppColor appColor = AppColor();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double fixSize = size.width + size.height;
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: 'Cash Book Details',
          fontSize: fixSize * 0.015,
        ),
      ),
      body: SizedBox(
        width: size.width,
        height: size.height,
        child: StreamBuilder<CashBookModel>(
            stream: getDataCashBook(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(child: CircularProgressIndicator());
              }
              return DefaultTabController(
                length: 3,
                child: Column(
                  children: [
                    SizedBox(
                      height: fixSize * 0.01,
                    ),
                    Container(
                        padding: EdgeInsets.all(fixSize * 0.015),
                        margin:
                            EdgeInsets.symmetric(horizontal: fixSize * 0.01),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: fixSize * 0.0025)
                            ],
                            borderRadius:
                                BorderRadius.circular(fixSize * 0.0025),
                            color: Colors.white),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomText(
                                    text: 'ຫົວຂໍ້: ',
                                    fontSize: fixSize * 0.0165,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  CustomText(
                                    text: snapshot.data!.title.toString(),
                                    fontSize: fixSize * 0.0165,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    text: 'ເງີນລວມ',
                                    fontSize: fixSize * 0.015,
                                  ),
                                  CustomText(
                                    text: FormatPrice(
                                            price: snapshot.data!.total ?? 0)
                                        .toString(),
                                    fontSize: fixSize * 0.015,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: fixSize * 0.0025,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    text: 'ເງີນໃຊ້ຈ່າຍ',
                                    fontSize: fixSize * 0.015,
                                  ),
                                  CustomText(
                                    text:
                                        '-${FormatPrice(price: snapshot.data!.spend ?? 0)}',
                                    fontSize: fixSize * 0.015,
                                    color: Colors.red,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: fixSize * 0.0025,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    text: 'ເງີນຍັງເຫຼືອ',
                                    fontSize: fixSize * 0.015,
                                  ),
                                  CustomText(
                                    text: FormatPrice(
                                            price: snapshot.data!.balance ?? 0)
                                        .toString(),
                                    fontSize: fixSize * 0.015,
                                    color: Colors.green,
                                  ),
                                ],
                              ),
                            ])),
                    SizedBox(
                      height: fixSize * 0.005,
                    ),
                    CustomText(
                      text: 'ລາຍການລາຍຮັບລາຍຈ່າຍ',
                      fontSize: fixSize * 0.0175,
                      fontWeight: FontWeight.w600,
                    ),
                    TabBar(tabs: [
                      Tab(
                        child: CustomText(
                          text: 'ລວມທັງໝົດ',
                          color: appColor.black,
                          fontSize: fixSize * 0.0145,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Tab(
                        child: CustomText(
                          text: 'ລາຍຮັບ',
                          color: appColor.black,
                          fontSize: fixSize * 0.0145,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Tab(
                        child: CustomText(
                          text: 'ລາຍຈ່າຍ',
                          color: appColor.black,
                          fontSize: fixSize * 0.0145,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ]),
                    Expanded(
                      child: TabBarView(
                        children: [
                          ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  horizontal: fixSize * 0.01),
                              shrinkWrap: true,
                              itemCount: snapshot.data!.listdetail!.length,
                              itemBuilder: (context, index) {
                                return Column(
                                  children: [
                                    ListTile(
                                      onLongPress: () {
                                        showDialogDelete(index);
                                      },
                                      title: CustomText(
                                        text: snapshot.data!.listdetail![index]
                                                    .type ==
                                                1
                                            ? 'ລາຍຮັບ'
                                            : 'ລາຍຈ່າຍ',
                                        fontSize: fixSize * 0.015,
                                      ),
                                      subtitle: CustomText(
                                        text: snapshot
                                            .data!.listdetail![index].descript
                                            .toString(),
                                        fontSize: fixSize * 0.0135,
                                      ),
                                      trailing: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          CustomText(
                                            text: dateFormat.format(
                                              DateTime.parse(
                                                snapshot.data!
                                                    .listdetail![index].time!
                                                    .toDate()
                                                    .toString(),
                                              ),
                                            ),
                                            fontSize: fixSize * 0.01,
                                          ),
                                          CustomText(
                                            text: FormatPrice(
                                                    price: snapshot
                                                            .data!
                                                            .listdetail![index]
                                                            .amount ??
                                                        0)
                                                .toString(),
                                            fontSize: fixSize * 0.0135,
                                            color: snapshot
                                                        .data!
                                                        .listdetail![index]
                                                        .type ==
                                                    1
                                                ? Colors.green
                                                : Colors.red,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                      height: 1,
                                    ),
                                    index ==
                                            snapshot.data!.listdetail!.length -
                                                1
                                        ? SizedBox(height: fixSize * 0.065)
                                        : const SizedBox(),
                                  ],
                                );
                              }),
                          ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  horizontal: fixSize * 0.01),
                              shrinkWrap: true,
                              itemCount: snapshot.data!.listdetail!.length,
                              itemBuilder: (context, index) {
                                if (snapshot.data!.listdetail![index].type !=
                                    1) {
                                  return const SizedBox();
                                }
                                return Column(
                                  children: [
                                    ListTile(
                                      onLongPress: () {
                                        showDialogDelete(index);
                                      },
                                      title: CustomText(
                                        text: snapshot.data!.listdetail![index]
                                                    .type ==
                                                1
                                            ? 'ລາຍຮັບ'
                                            : 'ລາຍຈ່າຍ',
                                        fontSize: fixSize * 0.015,
                                      ),
                                      subtitle: CustomText(
                                        text: snapshot
                                            .data!.listdetail![index].descript
                                            .toString(),
                                        fontSize: fixSize * 0.0135,
                                      ),
                                      trailing: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          CustomText(
                                            text: dateFormat.format(
                                              DateTime.parse(
                                                snapshot.data!
                                                    .listdetail![index].time!
                                                    .toDate()
                                                    .toString(),
                                              ),
                                            ),
                                            fontSize: fixSize * 0.01,
                                          ),
                                          CustomText(
                                            text: FormatPrice(
                                                    price: snapshot
                                                            .data!
                                                            .listdetail![index]
                                                            .amount ??
                                                        0)
                                                .toString(),
                                            fontSize: fixSize * 0.0135,
                                            color: snapshot
                                                        .data!
                                                        .listdetail![index]
                                                        .type ==
                                                    1
                                                ? Colors.green
                                                : Colors.red,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                      height: 1,
                                    ),
                                    index ==
                                            snapshot.data!.listdetail!.length -
                                                1
                                        ? SizedBox(height: fixSize * 0.065)
                                        : const SizedBox(),
                                  ],
                                );
                              }),
                          ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  horizontal: fixSize * 0.01),
                              shrinkWrap: true,
                              itemCount: snapshot.data!.listdetail!.length,
                              itemBuilder: (context, index) {
                                if (snapshot.data!.listdetail![index].type ==
                                    1) {
                                  return const SizedBox();
                                }
                                return Column(
                                  children: [
                                    ListTile(
                                      onLongPress: () {
                                        showDialogDelete(index);
                                      },
                                      title: CustomText(
                                        text: snapshot.data!.listdetail![index]
                                                    .type ==
                                                1
                                            ? 'ລາຍຮັບ'
                                            : 'ລາຍຈ່າຍ',
                                        fontSize: fixSize * 0.015,
                                      ),
                                      subtitle: CustomText(
                                        text: snapshot
                                            .data!.listdetail![index].descript
                                            .toString(),
                                        fontSize: fixSize * 0.0135,
                                      ),
                                      trailing: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          CustomText(
                                            text: dateFormat.format(
                                              DateTime.parse(
                                                snapshot.data!
                                                    .listdetail![index].time!
                                                    .toDate()
                                                    .toString(),
                                              ),
                                            ),
                                            fontSize: fixSize * 0.01,
                                          ),
                                          CustomText(
                                            text: FormatPrice(
                                                    price: snapshot
                                                            .data!
                                                            .listdetail![index]
                                                            .amount ??
                                                        0)
                                                .toString(),
                                            fontSize: fixSize * 0.0135,
                                            color: snapshot
                                                        .data!
                                                        .listdetail![index]
                                                        .type ==
                                                    1
                                                ? Colors.green
                                                : Colors.red,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                      height: 1,
                                    ),
                                    index ==
                                            snapshot.data!.listdetail!.length -
                                                1
                                        ? SizedBox(height: fixSize * 0.065)
                                        : const SizedBox(),
                                  ],
                                );
                              }),
                        ],
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialogAdd();
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: fixSize * 0.02,
        ),
      ),
    );
  }

  Stream<CashBookModel> getDataCashBook() async* {
    yield* FirebaseFirestore.instance
        .collection('cashBooks')
        .doc(widget.id)
        .snapshots()
        .map((snapShot) {
      listdetail = [];
      cashBookModel = CashBookModel.fromJson(snapShot.data()!);
      if (cashBookModel.listdetail != null) {
        for (var element in cashBookModel.listdetail!) {
          listdetail.add(element.toJson());
        }
      }
      return cashBookModel;
    });
  }

  addListDetail(Map<String, dynamic> newData) async {
    listdetail = [];
    for (var element in cashBookModel.listdetail!) {
      listdetail.add(element.toJson());
    }
    listdetail.add(newData);

    await FirebaseFirestore.instance
        .collection('cashBooks')
        .doc(widget.id)
        .update({'listdetail': listdetail}).then((value) {
      print('succes');
      getcheckPrice(listdetail);
    }).catchError((e) {
      print(e.toString());
    });
  }

  getcheckPrice(List<dynamic> value) async {
    num total = 0;
    num spend = 0;
    num balance = 0;
    for (var element in value) {
      if (element['type'] == 1) {
        total += element['amount'];
      } else {
        spend += element['amount'];
      }
    }
    balance = total - spend;

    await FirebaseFirestore.instance
        .collection('cashBooks')
        .doc(widget.id)
        .update({'spend': spend, 'balance': balance, 'total': total})
        .then((value) {})
        .catchError((e) {
          print(e.toString());
        });
  }

  showDialogDelete(int index) async {
    var res = await Get.dialog(Builder(builder: (context) {
      Size size = MediaQuery.of(context).size;
      double fixSize = size.width + size.height;
      return AlertDialog(
        title: Icon(
          Icons.question_mark_outlined,
          color: AppColor().mainColor,
          size: fixSize * 0.025,
        ),
        content: CustomText(
          text: 'ທ່ານຕ້ອງການລົບລາຍການນີ້ຫຼືບໍ່ ',
          fontSize: fixSize * 0.0165,
          fontWeight: FontWeight.w600,
        ),
        actions: [
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.grey)),
            onPressed: () {
              Get.back();
            },
            child: CustomText(
              text: 'ຍົກເລິກ',
              fontSize: fixSize * 0.0165,
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.blue)),
            onPressed: () {
              Get.back(result: true);
            },
            child: CustomText(
              text: 'ຕົກລົງ',
              fontSize: fixSize * 0.0165,
              color: Colors.white,
            ),
          ),
        ],
      );
    }));
    if (res == true) {
      deleteListdetail(index);
    }
  }

  deleteListdetail(int index) async {
    listdetail.removeAt(index);
    await FirebaseFirestore.instance
        .collection('cashBooks')
        .doc(widget.id)
        .update({'listdetail': listdetail}).then((value) {
      print('succes');
      getcheckPrice(listdetail);
    }).catchError((e) {
      print(e.toString());
    });
  }

  showDialogAdd() async {
    int valueRadio = 1;
    amountT.clear();
    descript.clear();
    var res = await Get.dialog(Builder(builder: (context) {
      Size size = MediaQuery.of(context).size;
      double fixSize = size.width + size.height;
      return AlertDialog(
        title: CustomText(
          text: 'ໃສ່ລາຍລະອຽດ ',
          fontSize: fixSize * 0.0165,
          fontWeight: FontWeight.w600,
        ),
        content: Column(mainAxisSize: MainAxisSize.min, children: [
          StatefulBuilder(
            builder: (context, setState) {
              return Row(
                children: [
                  Expanded(
                    child: RadioListTile<int>(
                      contentPadding: EdgeInsets.zero,
                      value: 1,
                      groupValue: valueRadio,
                      onChanged: (c) {
                        valueRadio = c!;
                        setState(() {});
                      },
                      title: CustomText(
                        text: 'ລາຍຮັບ',
                        fontSize: fixSize * 0.0125,
                      ),
                    ),
                  ),
                  Expanded(
                    child: RadioListTile<int>(
                      contentPadding: EdgeInsets.zero,
                      value: 2,
                      groupValue: valueRadio,
                      onChanged: (c) {
                        valueRadio = c!;
                        setState(() {});
                      },
                      title: CustomText(
                        text: 'ລາຍຈ່າຍ',
                        fontSize: fixSize * 0.0125,
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
          FieldNormalTextWidget(
            hintText: 'ຈຳນວນ',
            textController: amountT,
            keyBoardType: TextInputType.number,
          ),
          SizedBox(
            height: fixSize * 0.01,
          ),
          FieldNormalTextWidget(hintText: 'ລາຍລະອຽດ', textController: descript)
        ]),
        actions: [
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.grey)),
            onPressed: () {
              Get.back();
            },
            child: CustomText(
              text: 'ຍົກເລິກ',
              fontSize: fixSize * 0.0165,
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith((states) => Colors.blue)),
            onPressed: () {
              Get.back(result: true);
            },
            child: CustomText(
              text: 'ຕົກລົງ',
              fontSize: fixSize * 0.0165,
              color: Colors.white,
            ),
          ),
        ],
      );
    }));
    if (res == true) {
      if (amountT.text.isEmpty) {
        return;
      }
      Listdetail newData = Listdetail(
          amount: num.parse(amountT.text),
          descript: descript.text,
          time: Timestamp.now(),
          type: valueRadio);
      addListDetail(newData.toJson());
    }
  }
}
