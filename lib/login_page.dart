import 'package:cash_book/cash_book.dart';
import 'package:cash_book/register.dart';
import 'package:cash_book/states/profile_state.dart';
import 'package:cash_book/widgets/custom_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widgets/field_text_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailT = TextEditingController();
  TextEditingController passT = TextEditingController();
  ProfileState profileState = Get.put(ProfileState());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double fixSize = size.width + size.height;
    return Scaffold(
      body: SizedBox(
        width: size.width,
        height: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: size.width * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FieldTextWidget(
                    hintText: 'Email',
                    icon: Icons.email,
                    textController: emailT,
                    obscure: false,
                  ),
                  SizedBox(
                    height: fixSize * 0.01,
                  ),
                  FieldTextWidget(
                    hintText: 'Password',
                    icon: Icons.lock,
                    textController: passT,
                    obscure: true,
                  ),
                  SizedBox(
                    height: fixSize * 0.01,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: fixSize * 0.035,
                          child: ElevatedButton(
                              onPressed: () {
                                logincheck();
                              },
                              child: const CustomText(
                                text: 'Login',
                              )),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: fixSize * 0.01,
                  ),
                  TextButton(
                    onPressed: () {
                      Get.to(const RegisterPage());
                    },
                    child: CustomText(
                        text: 'Register for using',
                        fontSize: fixSize * 0.0125,
                        color: Colors.blue,
                        textDecoration: TextDecoration.underline),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> logincheck() async {
    if (emailT.text.isNotEmpty && passT.text.isNotEmpty) {
      try {
        await FirebaseAuth.instance
            .signInWithEmailAndPassword(
                email: emailT.text.trim(), password: passT.text.trim())
            .then((value) async {
          Get.off(const CashBookPage());
        });
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: CustomText(
          text: "ອີເມວ ແລະ ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ ກະລຸນາລອງໃຫມ່!",
          color: Colors.white,
        )));
        await FirebaseAuth.instance.signOut();
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: CustomText(
        text: "ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ",
        color: Colors.white,
      )));
    }
  }
}
