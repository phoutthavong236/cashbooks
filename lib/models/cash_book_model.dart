import 'package:cloud_firestore/cloud_firestore.dart';

class CashBookModel {
  String? id;
  String? title;
  num? total;
  num? spend;
  num? balance;
  String? userId;
  List<Listdetail>? listdetail;

  CashBookModel(
      {this.id,
      this.title,
      this.total,
      this.spend,
      this.balance,
      this.userId,
      this.listdetail});

  CashBookModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    total = json['total'];
    spend = json['spend'];
    balance = json['balance'];
    userId = json['user_id'];
    if (json['listdetail'] != null) {
      listdetail = <Listdetail>[];
      json['listdetail'].forEach((v) {
        listdetail!.add(Listdetail.fromJson(v));
      });
      if (listdetail != null) {
        listdetail?.sort((a, b) => b.time!.compareTo(a.time!));
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['total'] = total;
    data['spend'] = spend;
    data['balance'] = balance;
    data['user_id'] = userId;
    if (listdetail != null) {
      data['listdetail'] = listdetail!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Listdetail {
  num? amount;
  String? descript;
  Timestamp? time;
  int? type;

  Listdetail({this.amount, this.descript, this.time, this.type});

  Listdetail.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    descript = json['descript'];
    time = json['time'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['amount'] = amount;
    data['descript'] = descript;
    data['time'] = time;
    data['type'] = type;
    return data;
  }
}
