import 'package:cash_book/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class ProfileState extends GetxController {
  UserModel? userModel;

  setUser(UserModel model) {
    userModel = model;
    update();
  }

  getUser() async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .get()
        .then((value) {
      userModel = UserModel.fromJson(value.data() ?? {});
    });
    update();
  }

  @override
  void onReady() {
    getUser();
    super.onReady();
  }
}
